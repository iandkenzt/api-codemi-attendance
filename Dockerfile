FROM golang:1.9.2-alpine3.6 AS builder

# Install tools required to build the project
# We will need to run `docker build --no-cache .` to update those dependencies
RUN apk add --no-cache git
RUN go get github.com/golang/dep/cmd/dep

# Gopkg.toml and Gopkg.lock lists project dependencies
# These layers will only be re-built when Gopkg files are updated
COPY Gopkg.lock Gopkg.toml /go/src/gitlab.com/iandkenzt/api-codemi-attendance/
WORKDIR /go/src/gitlab.com/iandkenzt/api-codemi-attendance

# Install library dependencies
RUN dep ensure -vendor-only -v

# Copy all project and build it
# This layer will be rebuilt when ever a file has changed in the project directory
COPY . /go/src/gitlab.com/iandkenzt/api-codemi-attendance/
RUN env CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o build/api-codemi-attendance -v

########### Second Stage, Get CA-Certificate And TimeZone Info ##############
FROM alpine:latest as alpine

RUN apk --no-cache add tzdata ca-certificates

COPY --from=builder /go/src/gitlab.com/iandkenzt/api-codemi-attendance/build/api-codemi-attendance /api-codemi-attendance
COPY --from=builder /go/src/gitlab.com/iandkenzt/api-codemi-attendance/.env /.env

# tell how to run this container 
CMD ["./api-codemi-attendance"]

EXPOSE 3000