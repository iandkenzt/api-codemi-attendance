package main

import (
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	c "gitlab.com/iandkenzt/api-codemi-attendance/configuration"
	"gitlab.com/iandkenzt/api-codemi-attendance/utils"
)

func init() {
	// instantiate a logger
	utils.Logger.Out = os.Stdout
	utils.InitLogger()
}

func main() {

	port := "3000"

	if c.Conf.Port != "" {
		port = c.Conf.Port
	}

	utils.Logger.Info("Listening on port:", port)
	w := utils.Logger.Writer()

	restAPIRouter := RestAPIRouter()
	r := handlers.LoggingHandler(w, restAPIRouter)

	headers := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization", "X-Api-Key"})
	methods := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"})
	origins := handlers.AllowedOrigins([]string{"*"})

	http.ListenAndServe(":"+port, handlers.CORS(headers, methods, origins)(r))
}
