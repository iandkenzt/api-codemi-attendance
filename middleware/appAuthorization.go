package middleware

import (
	"net/http"

	"gitlab.com/iandkenzt/api-codemi-attendance/utils"
)

// AppAuthorizationMiddleWare check secret key
func AppAuthorizationMiddleWare(f http.HandlerFunc) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		apiKeyHeader := req.Header.Get("X-Api-Key")

		if apiKeyHeader == "" {
			utils.Response(res, 460, "Forbiden", nil, http.StatusForbidden, nil)
			return
		}

		isAppKeyValid := utils.ValidateAppKey(apiKeyHeader)

		if isAppKeyValid == false {
			utils.Response(res, 460, "Forbiden", nil, http.StatusForbidden, nil)
			return
		}
		f(res, req)
	}
}
