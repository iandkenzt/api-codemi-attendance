package configuration

import (
	"os"

	"github.com/joho/godotenv"
)

// Configuration Env Struct
type Configuration struct {
	Port          string
	APIPrefix     string
	APIVersion    string
	AppSecretKey  string
	MysqlDbName   string
	MysqlUser     string
	MysqlPassword string
	MysqlPort     string
	MysqlHost     string
}

// Conf Var
var Conf Configuration

// LoadEnv ...
func init() {

	if Conf == (Configuration{}) {
		godotenv.Load()

		// Service Configuration
		Conf.Port = os.Getenv("PORT")
		Conf.APIPrefix = os.Getenv("API_PREFIX")
		Conf.APIVersion = os.Getenv("API_VERSION")
		Conf.AppSecretKey = os.Getenv("APP_SECRET_KEY")

		// Mysql Configuration
		Conf.MysqlDbName = os.Getenv("MYSQL_DBNAME")
		Conf.MysqlUser = os.Getenv("MYSQL_USER")
		Conf.MysqlPassword = os.Getenv("MYSQL_PASSWORD")
		Conf.MysqlPort = os.Getenv("MYSQL_PORT")
		Conf.MysqlHost = os.Getenv("MYSQL_HOST")
	}

}
