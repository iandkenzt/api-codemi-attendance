package isalive

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/iandkenzt/api-codemi-attendance/utils"
)

// Alive Struct
type Alive struct {
	IsAlive bool `json:"is_alive"`
}

// IsAlive Check machine status
func IsAlive(res http.ResponseWriter, req *http.Request) {

	var status Alive
	status.IsAlive = true

	utils.Response(res, 0, "Codemi API service is a live", status, http.StatusOK, nil)
}

// BuildStatusRoutes ...
func BuildStatusRoutes(r *mux.Router) {
	r.Methods("GET").Path("/is_alive").HandlerFunc(IsAlive)
}
