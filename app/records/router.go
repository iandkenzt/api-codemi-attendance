package records

import (
	"github.com/gorilla/mux"

	"gitlab.com/iandkenzt/api-codemi-attendance/app/records/routes"
	m "gitlab.com/iandkenzt/api-codemi-attendance/middleware"
)

// BuildRecordsRoutes ...
func BuildRecordsRoutes(r *mux.Router) {
	// class
	r.Methods("GET").Path("/class").HandlerFunc(m.AppAuthorizationMiddleWare(routes.ClassGetAll))
	r.Methods("POST").Path("/class").HandlerFunc(m.AppAuthorizationMiddleWare(routes.ClassInsert))

	// participant
	r.Methods("GET").Path("/participant").HandlerFunc(m.AppAuthorizationMiddleWare(routes.ParticipantGetAll))
	r.Methods("POST").Path("/participant").HandlerFunc(m.AppAuthorizationMiddleWare(routes.ParticipantInsert))

	// attendance
	r.Methods("GET").Path("/attendance").HandlerFunc(m.AppAuthorizationMiddleWare(routes.AttendanceGetAll))
	r.Methods("POST").Path("/attendance").HandlerFunc(m.AppAuthorizationMiddleWare(routes.AttendanceInsert))
}
