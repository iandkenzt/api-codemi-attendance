package models

import (
	"errors"
	"fmt"

	"gitlab.com/iandkenzt/api-codemi-attendance/utils"
)

// ParticipantGetAll ...
func ParticipantGetAll(condition string) (class []interface{}, err error) {

	// get connection
	db, err := utils.GetConnectionDB()
	if err != nil {
		return class, err
	}
	defer db.Close()

	// query builder
	var queryBuilder string
	if condition != "" {
		queryBuilder = fmt.Sprintf(`
			SELECT id, email, full_name, first_name, last_name, phone_number, gender, address, company
			FROM participant %s
		`, condition)
	} else {
		queryBuilder = `
			SELECT id, email, full_name, first_name, last_name, phone_number, gender, address, company
			FROM participant
		`
	}

	// query data class
	rows, err := db.Query(queryBuilder)
	if err != nil {
		msg := fmt.Sprintf("Error when query data participant | %s", err)
		errMsg := errors.New(msg)
		utils.Logger.Errorf(errMsg.Error())

		return class, errMsg
	}
	defer rows.Close()

	// prepare all data class
	var result []interface{}
	for rows.Next() {
		var each = Participant{}
		var err = rows.Scan(&each.ID, &each.Email, &each.FullName, &each.FirstName,
			&each.LastName, &each.PhoneNumber, &each.Gender, &each.Address, &each.Company)

		if err != nil {
			msg := fmt.Sprintf("Error when prepare data participant | %s", err)
			errMsg := errors.New(msg)
			utils.Logger.Errorf(errMsg.Error())

			return class, errMsg
		}

		result = append(result, each)
	}

	return result, nil

}
