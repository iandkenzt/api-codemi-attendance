package models

import (
	"gitlab.com/iandkenzt/api-codemi-attendance/utils"
)

// ClassInsert ...
func ClassInsert(c Class) (status bool, err error) {

	// get connection
	db, err := utils.GetConnectionDB()
	if err != nil {
		return false, err
	}
	defer db.Close()

	// insert data
	_, err = db.Exec("insert into class (name, duration, date) VALUES (?, ?, ?)", c.Name, c.Duration, c.Date)
	if err != nil {
		return false, err
	}

	return false, nil

}
