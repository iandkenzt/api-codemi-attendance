package models

// Attendance Struct
type Attendance struct {
	ID                     int32  `json:"attendance_id"`
	DateTime               string `json:"attendance_date_time"`
	ClassID                int32  `json:"class_id"`
	ClassName              string `json:"class_name"`
	ClassDuration          string `json:"class_duration"`
	ClassDate              string `json:"class_date"`
	ParticipantID          int32  `json:"participant_id"`
	ParticipantEmail       string `json:"participant_email"`
	ParticipantFullName    string `json:"participant_full_name"`
	ParticipantFirstName   string `json:"participant_first_name"`
	ParticipantLastName    string `json:"participant_last_name"`
	ParticipantPhoneNumber string `json:"participant_phone_number"`
	ParticipantGender      string `json:"participant_gender"`
	ParticipantAddress     string `json:"participant_address"`
	ParticipantCompany     string `json:"participant_company"`
}
