package models

import (
	"time"

	"gitlab.com/iandkenzt/api-codemi-attendance/utils"
)

// AttendanceInsert ...
func AttendanceInsert(at Attendance) (status bool, err error) {

	// get connection
	db, err := utils.GetConnectionDB()
	if err != nil {
		return false, err
	}
	defer db.Close()

	// insert data
	dateTimeNow := utils.TimeToStrDate(time.Now())
	_, err = db.Exec("insert into attendance (class_id, participant_id, date_time) VALUES (?, ?, ?)",
		at.ClassID, at.ParticipantID, dateTimeNow)
	if err != nil {
		return false, err
	}

	return false, nil

}
