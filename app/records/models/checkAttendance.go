package models

import (
	"errors"
	"fmt"

	"gitlab.com/iandkenzt/api-codemi-attendance/utils"
)

// CheckAttendance ...
func CheckAttendance(classID int32, participantID int32) (status bool, err error) {

	// get connection
	db, err := utils.GetConnectionDB()
	if err != nil {
		return false, err
	}
	defer db.Close()

	query := fmt.Sprintf(`
		SELECT EXISTS(
			SELECT * FROM attendance WHERE class_id=%d AND participant_id=%d
		) AS check_attendance`, classID, participantID)

	rows, err := db.Query(query)
	if err != nil {

		msg := fmt.Sprintf("Error when check exist of email | %s", err)
		errMsg := errors.New(msg)
		utils.Logger.Errorf(errMsg.Error())

		return false, errMsg
	}
	defer rows.Close()

	var countClassID int32
	for rows.Next() {
		rows.Scan(&countClassID)
	}

	if countClassID > 0 {
		return true, nil
	}

	return false, nil

}
