package models

// Class Struct
type Class struct {
	ID       int32  `json:"id"`
	Name     string `json:"name"`
	Duration string `json:"duration"`
	Date     string `json:"date"`
}
