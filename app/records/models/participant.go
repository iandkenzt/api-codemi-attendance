package models

// Participant Struct
type Participant struct {
	ID          int32  `json:"id"`
	Email       string `json:"email"`
	FullName    string `json:"full_name"`
	FirstName   string `json:"first_name"`
	LastName    string `json:"last_name"`
	PhoneNumber string `json:"phone_number"`
	Gender      string `json:"gender"`
	Address     string `json:"address"`
	Company     string `json:"company"`
}
