package models

import (
	"errors"
	"fmt"

	"gitlab.com/iandkenzt/api-codemi-attendance/utils"
)

// ClassGetAll ...
func ClassGetAll(condition string) (class []interface{}, err error) {

	// get connection
	db, err := utils.GetConnectionDB()
	if err != nil {
		return class, err
	}
	defer db.Close()

	// query builder
	var queryBuilder string
	if condition != "" {
		queryBuilder = fmt.Sprintf("SELECT id, name, duration, date FROM class %s", condition)
	} else {
		queryBuilder = "SELECT id, name, duration, date FROM class"
	}

	// query data class
	rows, err := db.Query(queryBuilder)
	if err != nil {
		msg := fmt.Sprintf("Error when query all data class | %s", err)
		errMsg := errors.New(msg)
		utils.Logger.Errorf(errMsg.Error())

		return class, errMsg
	}
	defer rows.Close()

	// prepare all data class
	var result []interface{}
	for rows.Next() {
		var each = Class{}
		var err = rows.Scan(&each.ID, &each.Name, &each.Duration, &each.Date)

		if err != nil {
			msg := fmt.Sprintf("Error when prepare all data class | %s", err)
			errMsg := errors.New(msg)
			utils.Logger.Errorf(errMsg.Error())

			return class, errMsg
		}

		result = append(result, each)
	}

	return result, nil

}
