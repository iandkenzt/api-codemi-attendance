package models

import (
	"errors"
	"fmt"

	"gitlab.com/iandkenzt/api-codemi-attendance/utils"
)

// CheckParticipantEmail ...
func CheckParticipantEmail(email string) (status bool, participantID int32, err error) {

	// get connection
	db, err := utils.GetConnectionDB()
	if err != nil {
		return false, 0, err
	}
	defer db.Close()

	query := fmt.Sprintf(`SELECT id FROM participant WHERE email="%s"`, email)
	rows, err := db.Query(query)
	if err != nil {

		msg := fmt.Sprintf("Error when check exist of email | %s", err)
		errMsg := errors.New(msg)
		utils.Logger.Errorf(errMsg.Error())

		return false, 0, errMsg
	}
	defer rows.Close()

	var ID int32
	for rows.Next() {
		rows.Scan(&ID)
	}

	if ID > 0 {
		return false, ID, nil
	}

	return false, 0, nil

}
