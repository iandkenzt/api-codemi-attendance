package models

import (
	"errors"
	"fmt"

	"gitlab.com/iandkenzt/api-codemi-attendance/utils"
)

// CheckParticipantByEmail ...
func CheckParticipantByEmail(email string) (status bool, err error) {

	// get connection
	db, err := utils.GetConnectionDB()
	if err != nil {
		return true, err
	}
	defer db.Close()

	rows, err := db.Query("SELECT COUNT(*) FROM participant WHERE email = ?", email)
	if err != nil {

		msg := fmt.Sprintf("Error when check count data participant | %s", err)
		errMsg := errors.New(msg)
		utils.Logger.Errorf(errMsg.Error())

		return true, errMsg
	}
	defer rows.Close()

	var countEmail int32
	for rows.Next() {
		rows.Scan(&countEmail)
	}

	if countEmail > 0 {
		return true, nil
	}

	return false, nil

}
