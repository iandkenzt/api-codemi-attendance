package models

import (
	"errors"
	"fmt"

	"gitlab.com/iandkenzt/api-codemi-attendance/utils"
)

// AttendanceGetAll ...
func AttendanceGetAll(condition string) (class []interface{}, err error) {

	// get connection
	db, err := utils.GetConnectionDB()
	if err != nil {
		return class, err
	}
	defer db.Close()

	// query builder
	var queryBuilder string
	if condition != "" {
		queryBuilder = fmt.Sprintf(`
		SELECT attendance.id, class.id, participant_id, attendance.date_time, class.name, class.duration, 
			class.date, participant.email, participant.full_name, participant.first_name, participant.last_name, 
			participant.phone_number, participant.gender, participant.address, participant.company
		FROM attendance
		JOIN class ON class.id = class_id
		JOIN participant ON participant.id = participant_id
		WHERE %s;
		`, condition)
	} else {
		queryBuilder = `
			SELECT attendance.id, class.id, participant_id, attendance.date_time, class.name, class.duration, 
				class.date, participant.email, participant.full_name, participant.first_name, participant.last_name, 
				participant.phone_number, participant.gender, participant.address, participant.company
			FROM attendance
			JOIN class ON class.id = class_id
			JOIN participant ON participant.id = participant_id;
		`
	}

	// query data class
	rows, err := db.Query(queryBuilder)
	if err != nil {
		msg := fmt.Sprintf("Error when query data participant | %s", err)
		errMsg := errors.New(msg)
		utils.Logger.Errorf(errMsg.Error())

		return class, errMsg
	}
	defer rows.Close()

	// prepare all data class
	var result []interface{}
	for rows.Next() {
		var each = Attendance{}
		var err = rows.Scan(&each.ID, &each.ClassID, &each.ParticipantID, &each.DateTime, &each.ClassName, &each.ClassDuration, &each.ClassDate,
			&each.ParticipantEmail, &each.ParticipantFullName, &each.ParticipantFirstName, &each.ParticipantLastName, &each.ParticipantPhoneNumber,
			&each.ParticipantGender, &each.ParticipantAddress, &each.ParticipantCompany)

		if err != nil {
			msg := fmt.Sprintf("Error when prepare data attendance | %s", err)
			errMsg := errors.New(msg)
			utils.Logger.Errorf(errMsg.Error())

			return class, errMsg
		}

		result = append(result, each)
	}

	return result, nil

}
