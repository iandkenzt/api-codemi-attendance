package models

import (
	"gitlab.com/iandkenzt/api-codemi-attendance/utils"
)

// ParticipantInsert ...
func ParticipantInsert(p Participant) (status bool, err error) {

	// get connection
	db, err := utils.GetConnectionDB()
	if err != nil {
		return false, err
	}
	defer db.Close()

	// insert data
	_, err = db.Exec("insert into participant (email, full_name, first_name, last_name, phone_number, gender, address, company) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
		p.Email, p.FullName, p.FirstName, p.LastName, p.PhoneNumber, p.Gender, p.Address, p.Company)
	if err != nil {
		return false, err
	}

	return false, nil

}
