package models

import (
	"errors"
	"fmt"

	"gitlab.com/iandkenzt/api-codemi-attendance/utils"
)

// CheckParticipantID ...
func CheckParticipantID(classID int32) (status bool, err error) {

	// get connection
	db, err := utils.GetConnectionDB()
	if err != nil {
		return true, err
	}
	defer db.Close()

	rows, err := db.Query("SELECT COUNT(*) FROM participant WHERE id=?", classID)
	if err != nil {

		msg := fmt.Sprintf("Error when check participant_id | %s", err)
		errMsg := errors.New(msg)
		utils.Logger.Errorf(errMsg.Error())

		return true, errMsg
	}
	defer rows.Close()

	var countClassID int32
	for rows.Next() {
		rows.Scan(&countClassID)
	}

	if countClassID > 0 {
		return true, nil
	}

	return false, nil

}
