package controllers

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"

	m "gitlab.com/iandkenzt/api-codemi-attendance/app/records/models"
	"gitlab.com/iandkenzt/api-codemi-attendance/utils"
)

// ParticipantGetAll ...
func ParticipantGetAll(req *http.Request) (class []interface{}, err error) {

	// request data
	reqID := req.URL.Query().Get("id")
	reqEmail := req.URL.Query().Get("email")
	classID, _ := strconv.Atoi(reqID)

	// prepare and check condition query builder
	var condition = ""
	if reqID != "" && reqEmail != "" {
		condition = fmt.Sprintf(`WHERE id=%d AND email="%s"`, classID, reqEmail)
	}
	if reqID != "" && reqEmail == "" {
		condition = fmt.Sprintf(`WHERE id=%d`, classID)
	}
	if reqID == "" && reqEmail != "" {
		condition = fmt.Sprintf(`WHERE email="%s"`, reqEmail)
	}

	// get all data class to DB
	resuls, err := m.ParticipantGetAll(condition)
	if err != nil {
		msg := fmt.Sprintf("Error when get data participant | %s", err)
		errMsg := errors.New(msg)
		utils.Logger.Errorf(errMsg.Error())

		return class, errMsg
	}

	return resuls, nil
}
