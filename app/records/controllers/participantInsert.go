package controllers

import (
	"errors"
	"fmt"

	m "gitlab.com/iandkenzt/api-codemi-attendance/app/records/models"
	"gitlab.com/iandkenzt/api-codemi-attendance/utils"
)

// ParticipantInsert ...
func ParticipantInsert(t m.Participant) (status bool, err error) {

	// validation json data
	email := t.Email
	if email == "" {
		errMsg := errors.New("Missing field email")
		utils.Logger.Errorf(errMsg.Error())

		return false, errMsg
	}

	emailStatus, err := m.CheckParticipantByEmail(email)
	if emailStatus == true && err == nil {
		msg := errors.New("Email is exists")
		utils.Logger.Errorf(msg.Error())
		return false, nil
	}

	if err != nil {
		msg := errors.New("Error when check data participant to DB")
		utils.Logger.Errorf(msg.Error())
		return false, msg
	}

	// insert data to DB
	_, err = m.ParticipantInsert(t)
	if err != nil {
		msg := fmt.Sprintf("Error when insert data participant | %s", err)
		errMsg := errors.New(msg)
		utils.Logger.Errorf(errMsg.Error())

		return false, errMsg
	}

	return true, nil
}
