package controllers

import (
	"errors"
	"fmt"
	"net/http"

	m "gitlab.com/iandkenzt/api-codemi-attendance/app/records/models"
	"gitlab.com/iandkenzt/api-codemi-attendance/utils"
)

// AttendanceGetAll ...
func AttendanceGetAll(req *http.Request) (class []interface{}, err error) {

	// request data
	reqAttendanceID := req.URL.Query().Get("id")
	reqClassName := req.URL.Query().Get("class_name")
	reqParticpantEmail := req.URL.Query().Get("participant_email")

	// check and prepare condition query builder
	var condition = ""
	if reqAttendanceID != "" {
		condition = fmt.Sprintf(`attendance.id=%s`, reqAttendanceID)
	}
	if reqClassName != "" {
		condition = fmt.Sprintf(`class.name LIKE "%s%s%s"`, "%", reqClassName, "%")
	}
	if reqParticpantEmail != "" {
		condition = fmt.Sprintf(`participant.email="%s"`, reqParticpantEmail)
	}

	// get all data class to DB
	resuls, err := m.AttendanceGetAll(condition)
	if err != nil {
		msg := fmt.Sprintf("Error when get all data class | %s", err)
		errMsg := errors.New(msg)
		utils.Logger.Errorf(errMsg.Error())

		return class, errMsg
	}

	return resuls, nil
}
