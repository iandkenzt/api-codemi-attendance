package controllers

import (
	"errors"
	"fmt"

	m "gitlab.com/iandkenzt/api-codemi-attendance/app/records/models"
	"gitlab.com/iandkenzt/api-codemi-attendance/utils"
)

// ClassInsert ...
func ClassInsert(t m.Class) (status bool, err error) {

	// validation json data
	name := t.Name
	if name == "" {
		errMsg := errors.New("Missing field name")
		utils.Logger.Errorf(errMsg.Error())

		return false, errMsg
	}
	duration := t.Duration
	if duration == "" {
		errMsg := errors.New("Missing field duration")
		utils.Logger.Errorf(errMsg.Error())

		return false, errMsg
	}
	date := t.Date
	if date == "" {
		errMsg := errors.New("Missing field date")
		utils.Logger.Errorf(errMsg.Error())

		return false, errMsg
	}

	// insert data to DB
	_, err = m.ClassInsert(t)
	if err != nil {
		msg := fmt.Sprintf("Error when insert data class | %s", err)
		errMsg := errors.New(msg)
		utils.Logger.Errorf(errMsg.Error())

		return false, errMsg
	}

	return true, nil
}
