package controllers

import (
	"errors"
	"fmt"

	m "gitlab.com/iandkenzt/api-codemi-attendance/app/records/models"
	"gitlab.com/iandkenzt/api-codemi-attendance/utils"
)

// AttendanceInsert ...
func AttendanceInsert(t m.Attendance) (status bool, err error) {

	// validation json data
	classID := t.ClassID
	if classID == 0 {
		errMsg := errors.New("Missing field class_id")
		utils.Logger.Errorf(errMsg.Error())

		return false, errMsg
	}

	participantEmail := t.ParticipantEmail
	if participantEmail == "" {
		errMsg := errors.New("Missing field participant_email")
		utils.Logger.Errorf(errMsg.Error())

		return false, errMsg
	}

	// check class_id is exist to DB
	classIDStatus, err := m.CheckClassID(classID)
	if classIDStatus == false && err == nil {
		msg := errors.New("class_id doesn't exist")
		utils.Logger.Errorf(msg.Error())
		return false, msg
	}

	// check participant is exist in DB
	participantEmailStatus, participantID, err := m.CheckParticipantEmail(participantEmail)
	if participantEmailStatus == false && participantID == 0 && err == nil {
		msg := errors.New("participant_email doesn't exist, please Registration before take a class")
		utils.Logger.Errorf(msg.Error())
		return false, msg
	}

	// change data participant id
	t.ParticipantID = participantID

	// check participant attendance is exist
	attendanceStatus, err := m.CheckAttendance(t.ClassID, t.ParticipantID)
	if attendanceStatus == true && err == nil {
		msg := fmt.Sprintf("%s already exist in class attendance", participantEmail)
		msgErr := errors.New(msg)
		utils.Logger.Errorf(msgErr.Error())
		return false, msgErr
	}
	// insert data to DB
	_, err = m.AttendanceInsert(t)
	if err != nil {
		msg := fmt.Sprintf("Error when insert data participant | %s", err)
		errMsg := errors.New(msg)
		utils.Logger.Errorf(errMsg.Error())

		return false, errMsg
	}

	return true, nil
}
