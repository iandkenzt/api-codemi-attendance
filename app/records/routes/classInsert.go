package routes

import (
	"encoding/json"
	"net/http"

	c "gitlab.com/iandkenzt/api-codemi-attendance/app/records/controllers"
	m "gitlab.com/iandkenzt/api-codemi-attendance/app/records/models"
	"gitlab.com/iandkenzt/api-codemi-attendance/utils"
)

// ClassInsert ...
func ClassInsert(res http.ResponseWriter, req *http.Request) {

	// decode requests JSON to struct
	var t m.Class
	decoder := json.NewDecoder(req.Body)
	errDecoder := decoder.Decode(&t)
	if errDecoder != nil {
		utils.Response(res, 1, "Error decode JSON on routes ClassInsert", "", http.StatusBadRequest, nil)
	}

	// handle process insert class data
	_, errProcess := c.ClassInsert(t)
	if errProcess != nil {
		utils.Response(res, 1, errProcess, "", http.StatusBadRequest, nil)
	}

	utils.Response(res, 0, "Success", "", http.StatusOK, nil)

}
