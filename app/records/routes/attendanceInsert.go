package routes

import (
	"encoding/json"
	"net/http"

	c "gitlab.com/iandkenzt/api-codemi-attendance/app/records/controllers"
	m "gitlab.com/iandkenzt/api-codemi-attendance/app/records/models"
	"gitlab.com/iandkenzt/api-codemi-attendance/utils"
)

// AttendanceInsert ...
func AttendanceInsert(res http.ResponseWriter, req *http.Request) {

	// decode requests JSON to struct
	var t m.Attendance
	decoder := json.NewDecoder(req.Body)
	errDecoder := decoder.Decode(&t)
	if errDecoder != nil {
		utils.Response(res, 1, "Error decode JSON on AttendanceInsert", "", http.StatusBadRequest, nil)
		return
	}

	// handle process insert Attendance data
	_, errProcess := c.AttendanceInsert(t)
	if errProcess != nil {
		msg := errProcess.Error()
		utils.Response(res, 1, msg, "", http.StatusBadRequest, nil)
		return
	}

	utils.Response(res, 0, "Success", "", http.StatusOK, nil)

}
