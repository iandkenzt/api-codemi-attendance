package routes

import (
	"net/http"

	c "gitlab.com/iandkenzt/api-codemi-attendance/app/records/controllers"
	"gitlab.com/iandkenzt/api-codemi-attendance/utils"
)

// AttendanceGetAll ...
func AttendanceGetAll(res http.ResponseWriter, req *http.Request) {

	// handle process get Participant data
	results, err := c.AttendanceGetAll(req)
	if err != nil {
		utils.Response(res, 1, err, "", http.StatusBadRequest, nil)
		return
	}

	if results == nil {
		utils.Response(res, 0, "Success", [...]string{""}, http.StatusOK, nil)
		return
	}

	utils.Response(res, 0, "Success", results, http.StatusOK, nil)

}
