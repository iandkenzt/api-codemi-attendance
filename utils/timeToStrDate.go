package utils

import "time"

// TimeToStrDate layout = "2006-01-02T15:04:05-0700"
func TimeToStrDate(t time.Time) string {
	var layout string
	var strDate string

	layout = "2006-01-02T15:04:05-0700"
	strDate = t.Format(layout)

	return strDate
}
