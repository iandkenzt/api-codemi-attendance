package utils

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"

	c "gitlab.com/iandkenzt/api-codemi-attendance/configuration"
	"gopkg.in/mgo.v2"
)

var mgoSession *mgo.Session

// GetConnectionDB ...
func GetConnectionDB() (*sql.DB, error) {

	mysqlURI := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s",
		c.Conf.MysqlUser, c.Conf.MysqlPassword, c.Conf.MysqlHost, c.Conf.MysqlPort, c.Conf.MysqlDbName)

	db, err := sql.Open("mysql", mysqlURI)
	if err != nil {
		return nil, err
	}

	return db, nil

}
