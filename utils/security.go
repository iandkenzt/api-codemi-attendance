package utils

import (
	c "gitlab.com/iandkenzt/api-codemi-attendance/configuration"
)

// ValidateAppKey check given key and compare with app key from env
func ValidateAppKey(myKey string) bool {
	apiSecretKey := c.Conf.AppSecretKey

	if myKey == apiSecretKey {
		// fmt.Println("Your token is valid.  I like your style.")
		return true
	}

	// fmt.Println("This token is terrible!  I cannot accept this.")
	return false
}
