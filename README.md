# api-codemi-attendance

This is services for handling record participant attendance of the class. Technologies used in this project, for backend used Golang and Database used Mysql.

## Pipeline Status

Master: [![pipeline status](https://gitlab.com/iandkenzt/api-codemi-attendance/badges/master/pipeline.svg)](https://gitlab.com/iandkenzt/api-codemi-attendance/commits/master)

## Getting Started

* copy .env.example to .env and edit accordingly

## Setup Database Environment

Create Privileges
```
mysql -u root -p
mysql> CREATE USER 'codemi'@'%' IDENTIFIED BY 'codemi';
mysql> GRANT ALL PRIVILEGES ON * . * TO 'codemi'@'%';
mysql> FLUSH PRIVILEGES;
```

Create Database and table
```
mysql -u codemi -p
mysql> CREATE DATABASE codemi;
mysql> USE codemi;
mysql> CREATE TABLE class ( id INTEGER PRIMARY KEY AUTO_INCREMENT, name TEXT, duration TEXT, date TEXT );
mysql> CREATE TABLE participant ( id INTEGER PRIMARY KEY AUTO_INCREMENT, email TEXT, full_name TEXT, first_name TEXT, last_name TEXT, phone_number TEXT, gender TEXT, address TEXT, company TEXT );
mysql> CREATE TABLE attendance (id INT PRIMARY KEY AUTO_INCREMENT, class_id INT, participant_id INT, date_time TEXT);
mysql> ALTER TABLE attendance ADD FOREIGN KEY (class_id) REFERENCES class(id);
mysql> ALTER TABLE attendance ADD FOREIGN KEY (participant_id) REFERENCES participant(id);
```

## How to run

### With Native Go

* Install Dependency Management
> `go get -u github.com/golang/dep/cmd/dep`

* Install Depedencies
> `dep ensure -v`

* Run Package
> `go run ./*.go`

## Request Example

```
curl --request GET \
  --url http://localhost:3000/api/v1/is_alive
```

## How to testing API Using Postman

Please read this document postman for testing guidance.

Guidance for local environment:
`https://documenter.getpostman.com/view/3482998/RzZ1rNiH`

Guidance for production environment:
`https://documenter.getpostman.com/view/3482998/RzZ1rNiJ`

If running in local environment used protocol domain `http://0.0.0.0:3000` and if you want to test in production server please use protocol domain `https://api-codemi-attendance.iandkenzt.com`.


## How to testing API Using User Inteface

I created a simple user interface using VueJS to integrated APIs and Frontend. Please visit `https://codemi-attendance.iandkenzt.com` for testing implementation APIs on frontend.

## Scenario for testing
1. Insert/registration Class
2. Insert/registration Participant
3. Insert Attendance
4. Get data Class, Participant, and Attendance