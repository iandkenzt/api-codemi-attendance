package main

import (
	"github.com/gorilla/mux"

	"gitlab.com/iandkenzt/api-codemi-attendance/app/records"
	status "gitlab.com/iandkenzt/api-codemi-attendance/app/status"
	c "gitlab.com/iandkenzt/api-codemi-attendance/configuration"
)

// RestAPIRouter ...
func RestAPIRouter() *mux.Router {
	// setup prefix and version
	apiPrefix := c.Conf.APIPrefix
	apiVersion := c.Conf.APIVersion

	// create a new router
	router := mux.NewRouter()
	apiRouter := router.PathPrefix(apiPrefix + apiVersion).Subrouter()

	// registration blueprint route
	status.BuildStatusRoutes(apiRouter)
	records.BuildRecordsRoutes(apiRouter)

	return router

}
